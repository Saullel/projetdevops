# ProjetDevops


[![codecov](https://codecov.io/gl/Saullel/projetdevops/branch/master/graph/badge.svg?token=RCW4BUZ8CM)](https://codecov.io/gl/Saullel/projetdevops)

[![pipeline](https://gitlab.com/Saullel/projetdevops/badges/master/pipeline.svg)](https://gitlab.com/Saullel/projetdevops/pipelines)

# Fonctionnalités implémentées

## Cette librairie propose les fonctionnalités suivates :

### Serie
   * Créer des Series à partir d'un tableau de String, avec ou sans label (transformation en type INT, FLOAT ou STRING selon les valeurs)
   * Construire une Serie à partir d'un sous ensemble de la Serie
   * Récupérer ou modifier le label de la Serie
   * Récupérer un élément à partir de son index
   * Récupérer la taille et le type de la Serie
   * Faire des opérations de moyenne et de variance sur des valeurs de type INT et FLOAT
   * Afficher la Serie entière ou une partie (début ou fin)
   * Calculer le minimum et le maximum d'une serie
 ### DataFrame  
   * Créer des DataFrame à partir de tableaux de String, avec ou sans labels (transformation en type INT, FLOAT ou STRING selon les valeurs)
   * Créer des DataFrame à partir de fichiers CSV
   * Construire un DataFrame avec un sous-ensemble du DataFrame (sélection de lignes)
   * Récupérer une Serie avec son label (sélection d'une colonne)
   * Afficher le DataFrame en entier ou une partie (début ou fin)
   * Afficher le type de chaque Serie du DataFrame
   
## Outils utilisés

Les tests unitaires ont été fait avec JUnit4.

Nous avons créé un projet Maven, qui incluait JaCoCo pour gérer la couverture de code. 

Nous utilisons CI, fournie par Gitlab. 
Celle-ci lance les tests, envoie le rapport de couverture du code à Codecov.io.

Si les tests sont passés avec succès, nous construisons une image docker, que nous envoyons sur le container registery.
L'application est juste un affichage d'un dataframe, contenant deux Series.

Pour consulter la documentation :
[javadoc](https://saullel.gitlab.io/-/projetdevops/-/jobs/532606467/artifacts/public/apidocs/index.html)