import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;

import org.apache.commons.lang.NullArgumentException;

import Exception.*;

import static java.lang.System.exit;

/***
 * This class allow to create a frame of datas and print them, select a part of them.
 *
 * @author François RObert, Laura SAULLE
 *
 */
public class DataFrame {
	ArrayList<Serie> data = new ArrayList<Serie>(); // contains the datas
	int max;
	
	public DataFrame() {
		this.max=0;
	}

	/***
	 *
	 * @param values values to fill the DataFrame
	 * @param labels labels of Series that compose the DataFrame
	 * @throws InputErrorException if columns don't have the same lenght
	 * @throws NullArgumentException if arguments are incorrect
	 */
	public DataFrame(String[][] values, String[] labels) throws InputErrorException,NullArgumentException {
		//Throw an exception is label==nul
		if(labels==null)
			throw new NullArgumentException("String [] labels is null");
		if(values==null)
			throw new NullArgumentException("String[][] values is null");
		//Throw an exception if the label [] isn't correct
		if(labels.length!=values.length) {
			throw new InputErrorException("String [] labels and String[][] values shall have the same lenght");
		}
		//To throw an exception if one column does not have the same lenght as the others
		int size_of_col;
		int size_of_prev_col=-1;
		//We parcour the whole values that are given, for each column of values we add a new column in our dataframe
		this.max = 0;
		int cpt = 0;
		for ( String [] col : values) {
			//Test on the lenght before creating anything
			size_of_col=col.length;
			if(size_of_prev_col!=-1 && size_of_prev_col!=size_of_col)
				throw new InputErrorException("The given String[][] values isn't correct: all columns shall have the same lenght");
			else {
				//We add a new column to the array list of column
				Serie serie = new Serie(col, labels[cpt]);
				data.add(serie);
				max = Math.max(max,serie.size());
				cpt++;
			}
			size_of_prev_col=size_of_col;
		}
	}

	/***
	 *
	 * @param values values to fill the DataFrame
	 * @throws InputErrorException if columns don't have the same lenght
	 * @throws NullArgumentException if arguments are incorrect
	 */
	public DataFrame(String[][] values) throws InputErrorException,NullArgumentException{
		if(values==null)
			throw new NullArgumentException("String[][] values is null");
		this.max = 0;
		int cpt = 0;
		String label = "default";
		//To throw an exception if one column does not have the same lenght as the others
		int size_of_col;
		int size_of_prev_col=-1;
		for ( String [] col : values) {//Test on the lenght before creating anything
			size_of_col=col.length;
			if(size_of_prev_col!=-1 && size_of_prev_col!=size_of_col)
				throw new InputErrorException("The given String[][] values isn't correct: all columns shall have the same lenght");
			else {
			//We add a new column to the array list of column
			Serie serie = new Serie(col,label + cpt);
			data.add(serie);
			max = Math.max(max,serie.size());
			cpt++;
			}
			size_of_prev_col=size_of_col;
		}
	}

	/***
	 *
	 * @param csvFile file to get values in order to fill the DataFrame
	 * @throws IOException if the file is empty
	 * @throws NullArgumentException if the file is null
	 * @throws InputErrorException if columns don't have the same lenght
	 */
	public DataFrame (String csvFile) throws IOException, NullArgumentException, InputErrorException {
		int i=0;
		BufferedReader buff=new BufferedReader(new FileReader(csvFile));
		int size_of_col=0;
		int size_of_prev_col=-1;
		String line;
		line=buff.readLine();
		String label = "default";
		while(line != null ) {
			String []values=line.split(",");					
			size_of_col=values.length;
			if(size_of_prev_col!=-1 && size_of_prev_col!=size_of_col)
				throw new InputErrorException("The file format isn't correct: all lines shall have the same lenght");
			size_of_prev_col=size_of_col;
			for(String s: values)
				if(s.isEmpty())
					throw new InputErrorException("No empty values admitted for datas");
			Serie serie =new Serie(values,label+i);
			data.add(serie);
			line=buff.readLine();
			max = Math.max(max,serie.size());
			i++;
		}
		if(i==0)
			throw new InputErrorException("The file should not be empty");
		buff.close();
	}

	/***
	 *
	 * @param csvFile file to get values in order to fill the DataFrame
	 * @param islabel if the csv file has label in the first line
	 * @throws IOException if the file is empty
	 * @throws NullArgumentException if the file is null
	 * @throws InputErrorException if columns don't have the same lenght
	 */
	public DataFrame (String csvFile,boolean islabel) throws IOException, NullArgumentException, InputErrorException {
		if(islabel) {
			int i=0;
			BufferedReader buff=new BufferedReader(new FileReader(csvFile));
			String line;
			line=buff.readLine();
			String []labels= {};
			int size_of_col=0;
			int size_of_prev_col=-1;
			while(line != null ) {
				if(i==0) {
					//the first line defines the labels //TODO : the first of each line is the label ?
					labels=line.split(",");
					for(String s: labels)
						if(s.isEmpty())
							throw new InputErrorException("No empty values admitted for labels");
				}
				else {
					if(i>labels.length) {
						//System.out.println(i);
						//System.out.println(labels.length);
						throw new InputErrorException("There is less labels than columns");
					}
					String []values=line.split(",");
					size_of_col=values.length;
					if(size_of_prev_col!=-1 && size_of_prev_col!=size_of_col)
						throw new InputErrorException("The file format isn't correct: all lines shall have the same lenght");
					for(String s: values)
						if(s.isEmpty())
							throw new InputErrorException("No empty values admitted for datas");
					Serie serie =new Serie(values,labels[i-1]);
					data.add(serie);
					max = Math.max(max,serie.size());
					size_of_prev_col=size_of_col;
				}
				line=buff.readLine();
				i++;
			}
			if(i==0)
				throw new InputErrorException("The file should not be empty");
			if(i==1)
					throw new InputErrorException("The file should have value, not only labels");
			buff.close();
			if(i<=labels.length)
				throw new InputErrorException("There is more labels than columns");
		}
		else new DataFrame(csvFile);
	}

	/***
	 *
	 * @param o objet to compatre the DataFrame
	 * @return true if the DataFrame are equals, false otherwise
	 */
	@Override
	public boolean equals (Object o) {
		if (o instanceof DataFrame) {
			if(this.data.size()!= ((DataFrame)o).data.size())
				return false;
			for (int i=0;i<data.size();i++)
				if(!this.data.get(i).equals(((DataFrame)o).data.get(i)))
					return false;
			return true;
		}
		return false;
	}


	/* -------------------------------------------------------------------------------------------------------------*/
	/*                                          Fonctions d'affichage                                               */
	/* -------------------------------------------------------------------------------------------------------------*/

	/***
	 *
	 * @param first the first index to print
	 * @param last the last index to print
	 * @return the correct presentation to print the DataFrame's lines from first to last
	 * @throws IndexOutOfBoundsException if arguments are incorrect
	 */
	private String getLinesFromTo(int first, int last) throws IndexOutOfBoundsException{
		if(first < 0) throw new IndexOutOfBoundsException("index not allowed : " + first);
		if(first > last) throw new IndexOutOfBoundsException("index not allowed : " + first + " < " + last);
		if(last > data.get(0).size()) throw new IndexOutOfBoundsException("index not allowed" + last);

		StringBuilder sb = new StringBuilder("");
		int nbLineWritten = 0;
		for(int i = first; i < last; i++){
			if(nbLineWritten%10 == 0){
				sb.append("\n");
				for(Serie serie : data)  sb.append("\t").append(serie.getLabel());
				sb.append("\n");
			}
			sb.append(i).append("\t");
			for (Serie serie : data){
				sb.append(serie.getVal(i)).append("\t");
			}
			sb.append("\n");
			nbLineWritten++;
		}
		return sb.toString();
	}

	/***
	 *
	 * @return the correct presentation to print the DataFrame
	 */
	public String toString(){
		return getLinesFromTo(0,data.get(0).size());
	}

	/***
	 * Print the entire DataFrame
	 */
	public void print(){
		System.out.println(this);
	}

	/***
	 *
	 * @param n first lines to print
	 * @throws IndexOutOfBoundsException if n incorrect
	 */
	public void head(int n) throws IndexOutOfBoundsException {
		System.out.println(getLinesFromTo(0, n));
	}

	/***
	 * Print the 5 first lines, or the entire DataFrame if there are less than 5 lines
	 * @throws IndexOutOfBoundsException never reached
	 */
	public void head() throws IndexOutOfBoundsException {
		int n;
		n = Math.min(data.get(0).size(), 5);
		System.out.println(getLinesFromTo(0, n));
	}

	/***
	 *
	 * @param n last lines to print
	 * @throws IndexOutOfBoundsException if n is incorrect
	 */
	public void tail(int n) throws IndexOutOfBoundsException {
		n = data.get(0).size() - n;
		System.out.println(getLinesFromTo(n, data.get(0).size()));
	}

	/***
	 * Print the 5 last lines, or the entire DataFrame if there are less than 5 lines
	 * @throws IndexOutOfBoundsException never reached
	 */
	public void tail() throws IndexOutOfBoundsException {
		int n;
		if(data.get(0).size() < 5) n = 0;
		else n = data.get(0).size() - 5;
		System.out.println(getLinesFromTo(n, data.get(0).size()));
	}

	/***
	 * Print type of each Serie which compose the DataFrame
	 */
	public void dtypes(){
		for(Serie serie : data) {
			System.out.println(serie.getLabel() + "\t" + serie.getType());
		}

	}

	/* -------------------------------------------------------------------------------------------------------------*/
	/*                                      Fonctions de selection                                                  */
	/* -------------------------------------------------------------------------------------------------------------*/

	/***
	 *
	 * @param label label of the Serie to find
	 * @return the first Serie which has the label given in argument
	 * @throws NotFoundException if there is no Serie whith this label
	 */
	public Serie getSerie(String label) throws NotFoundException {
		for(Serie serie : data){
			if (!label.equalsIgnoreCase(serie.getLabel())) {
				continue;
			}
			return serie;
		}
		throw new NotFoundException("this serie doesn't exist : " + label);
	}

	/***
	 *
	 * @param first index of the first element of the sub DataFrame
	 * @param last index of the last element of the sub DataFrame
	 * @return sub Dataframe composed with the element from first to last
	 * @throws IndexOutOfBoundsException if arguments are incorrect
	 */
	public DataFrame getSubData(int first, int last) throws IndexOutOfBoundsException{
		if(first < 0) throw new IndexOutOfBoundsException("index not allowed : " + first);
		if(first > last) throw new IndexOutOfBoundsException("index not allowed : " + first + " < " + last);
		if(last > data.get(0).size()) throw new IndexOutOfBoundsException("index not allowed" + last);

		DataFrame subData = new DataFrame();
		for(Serie serie : data) {
			Serie subSerie = serie.getSubSerie(first,last);
			subData.data.add(subSerie);
			subData.max = Math.max(max,subSerie.size());
		}
		return subData;
	}
}
