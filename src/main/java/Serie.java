import java.util.ArrayList;

import org.apache.commons.lang.NullArgumentException;

import static java.lang.System.exit;

import Exception.InputErrorException;
import Exception.TypeErrorException;

enum Data_type{FLOAT,INT,STRING}

/***
 * This class allows to create a frame of elements in order to print them, select a few of them or do some statistical calculation.
 *
 * @author François ROBERT, Laura SAULLE
 */
public class Serie {

    private String label;
    private ArrayList<String> values;
    private int maxLengthToPrint;
    private Data_type type;

    public Serie(){
        this.label = "default";
        maxLengthToPrint = this.label.length();
        type=Data_type.STRING;
        values= new ArrayList<>();
    }

    public Serie(String[] values) throws NullArgumentException, InputErrorException{
        this.label = "default";
        maxLengthToPrint = this.label.length();
        createTab(values);
    }

    public Serie(String[] values, String label) throws NullArgumentException, InputErrorException{
    	if (label ==null)
    		throw new NullArgumentException("The label is null");
    	if(label.isEmpty())
    		throw new InputErrorException("The String label is empty");
        this.label = label;
        maxLengthToPrint = label.length();
        createTab(values);
    }
    
    private void createTab(String[] values) throws InputErrorException,NullArgumentException{
        //Test on values 
    	if (values ==null)
    		throw new NullArgumentException("The String[] values is null");
    	// default type INT
        type = Data_type.INT;

        this.values = new ArrayList<>();
        for(String s : values) {
        	if (s.isEmpty()) throw new InputErrorException("a value in the String[] values is empty");
            //this.values.add(s);
            if (maxLengthToPrint < s.length()) maxLengthToPrint = s.length();
            switch (type) {
                case INT:
                    if (!isNumber(s)) {
                        if (isFloat(s)) type = Data_type.FLOAT;
                        else type = Data_type.STRING;
                    }
                    break;
                case FLOAT:
                    if (!isFloat(s)) type = Data_type.STRING;
                    break;
                default:
                    break;
            }
        }

        //Alignement du label et des valeurs
        StringBuilder s = new StringBuilder("");
        for(int i = 0; i < maxLengthToPrint-label.length(); i++) s.append(" ");
        s.append(label);
        this.label = s.toString();

        for(String val : values){
            int i;
            StringBuilder sb = new StringBuilder("");
            for( i = 0; i < maxLengthToPrint-val.length(); i++) sb.append(" ");
            sb.append(val);
            this.values.add(sb.toString());
        }
    }

    /***
     *
     * @return the label of the Serie
     */
    public String getLabel() {
        return label;
    }

    /***
     * Modifies the Serie's label
     * @param label toinit the Serie's label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /***
     *
     * @param index element's index to get
     * @return the values at the specified index
     */
    public String getVal(int index) {
        return values.get(index);
    }

    /***
     *
     * @return the size of the Serie
     */
    public int size() {
        return values.size();
    }

    /***
     *
     * @return the type of the Serie. It could be STRING, INT or FLOAT
     */
    public String getType() {
        String s;
        switch (type) {
            case INT:
                s = "INT";
                break;
            case FLOAT:
                s = "FLOAT";
                break;
            default:
                s = "STRING";
                break;
        }
        return s;
    }

    @Override
    public boolean equals (Object o) {
		if (o instanceof Serie) {
			if(!this.label.trim().equals(((Serie)o).label.trim()))
				return false;
			if(!this.getType().equals(((Serie)o).getType()))
				return false;
			if(this.values.size()!=((Serie)o).values.size())
				return false;
			for(int i=0;i<this.values.size();i++) {
				if(!this.values.get(i).trim().equals(((Serie)o).values.get(i).trim()))
					return false;
			}
			return true;
		}
		return false;
	}

    /* -------------------------------------------------------------------------------------------------------------*/
    /*                                      Fonctions d'affichage                                                   */
    /* -------------------------------------------------------------------------------------------------------------*/

    /***
     *
     * @param first the first index to print
     * @param last the last index to print
     * @return the correct presentation to print the Serie's lines from first to last
     * @throws IndexOutOfBoundsException if arguments are incorrect
     */
    private String getLinesFromTo(int first, int last) throws IndexOutOfBoundsException{
        if(first < 0) throw new IndexOutOfBoundsException("index not allowed : " + first);
        if(first > last) throw new IndexOutOfBoundsException("index not allowed : " + first + " < " + last);
        if(last > values.size()) throw new IndexOutOfBoundsException("index not allowed" + last);

        StringBuilder s = new StringBuilder("\t" + label);
        s.append("\n");
        for(int i = first; i < last; i++) {
            s.append(i).append("\t").append(values.get(i)).append("\n");
        }
        return s.toString();
    }

    /***
     *
     * @param n first lines to print
     * @throws IndexOutOfBoundsException if n incorrect
     */
    public void head(int n) throws IndexOutOfBoundsException{
        System.out.println(getLinesFromTo(0, n));
    }

    /***
     * Print the 5 first lines, or the entire Serie if there are less than 5 lines
     * @throws IndexOutOfBoundsException neverreached
     */
    public void head() throws IndexOutOfBoundsException {
        int n;
        n = Math.min(values.size(), 5);
        System.out.println(getLinesFromTo(0, n));
    }

    /***
     *
     * @param n last lines to print
     * @throws IndexOutOfBoundsException if n is incorrect
     */
    public void tail(int n) throws IndexOutOfBoundsException {
        n = values.size() - n;
        System.out.println(getLinesFromTo(n, values.size()));
    }

    /***
     * Print the 5 last lines, or the entire Serie if there are less than 5 lines
     * @throws IndexOutOfBoundsException never reached
     */
    public void tail() throws IndexOutOfBoundsException {
        int n;
        if(values.size() < 5) n = 0;
        else n = values.size() - 5;
        System.out.println(getLinesFromTo(n, values.size()));
    }

    /***
     * Print the entire Serie
     */
    public void print() {
        System.out.println(getLinesFromTo(0, values.size()));
    }

    /***
     *
     * @return the correct presentation to print the Serie
     */
    public String toString() {
        return getLinesFromTo(0, values.size());
    }

    /* -------------------------------------------------------------------------------------------------------------*/
    /*                                      Fonction de selection                                                   */
    /* -------------------------------------------------------------------------------------------------------------*/

    /***
     *
     * @param first index of the first element to get
     * @param last index of the last element to get
     * @return the sub Serie from the index first to index last. Returns null if there is a problem
     * @throws IndexOutOfBoundsException if arguments are incorrect
     */
    public Serie getSubSerie(int first, int last) throws IndexOutOfBoundsException {
        if(first < 0) throw new IndexOutOfBoundsException("index not allowed : " + first);
        if(first > last) throw new IndexOutOfBoundsException("index not allowed : " + first + " < " + last);
        if(last > values.size()) throw new IndexOutOfBoundsException("index not allowed" + last);

        String[] val = new String[last-first+1];
        for(int i = first; i <= last; i++){
            val[i-first] = values.get(i);
        }
        try {
            return new Serie(val,label);
        } catch (InputErrorException e) {
            e.printStackTrace();
            return null;
        }
    }

    /* -------------------------------------------------------------------------------------------------------------*/
    /*                                      Fonctions statistiques                                                  */
    /* -------------------------------------------------------------------------------------------------------------*/

    /***
     *
     * @return the number of elements in the Serie
     */
    public double count() {
        return values.size();
    }

    /***
     *
     * @return the mean of the Serie
     * @throws TypeErrorException if the Serie is type STRING
     */
    public double mean() throws TypeErrorException {
        if(type == Data_type.STRING) throw new TypeErrorException("Bad Serie type");

        double mean = 0;

        for (String value : values) mean += Double.parseDouble(value);
        mean = mean/values.size();
        return  mean;
    }

    /***
     *
     * @return the statistical variance
     * @throws TypeErrorException if the Serie is type STRING
     */
    public double sd() throws TypeErrorException {
        if(type == Data_type.STRING) throw new TypeErrorException("Bad Serie type");

        double mean = this.mean();
        double sd = 0;
        double x;

        for (String value : values) {
            x = Double.parseDouble(value);
            sd += Math.pow((x - mean), 2);
        }
        sd = Math.sqrt(sd/(values.size()));
        return  sd;
    }

    /***
     *
     * @return the Serie's minimum element
     * @throws TypeErrorException if the Serie is type STRING
     */
    public double min() throws TypeErrorException {
        if(type == Data_type.STRING) throw new TypeErrorException("Bad Serie type");

        double min = Double.parseDouble(values.get(0));
        double x;
        for (String value : values) {
            x = Double.parseDouble(value);
            min = Math.min(min,x);
        }
        return min;
    }

    /***
     *
     * @return the Serie's maximum element
     * @throws TypeErrorException if the Serie is type STRING
     */
    public double max() throws TypeErrorException {
        if(type == Data_type.STRING) throw new TypeErrorException("Bad Serie type");

        double max = Double.parseDouble(values.get(0));
        double x;
        for (String value : values) {
            x = Double.parseDouble(value);
            max = Math.max(max,x);
        }
        return max;
    }

    /* -------------------------------------------------------------------------------------------------------------*/

    private boolean isNumber(String input) {
        try {
            Integer.parseInt(input);
        } catch(NumberFormatException ex) {
            return false;
        }
        return true;
    }

    private boolean isFloat(String input) {
        try {
            Float.parseFloat(input);
        } catch(NumberFormatException ex) {
            return false;
        }
        return true;
    }
}
