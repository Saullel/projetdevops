package Exception;

public class InputErrorException extends Exception{
	public InputErrorException (String messageError) {
		super(messageError);
	}
}
