package Exception;

public class TypeErrorException extends Exception{

    public TypeErrorException(String messageError){
        super(messageError);
    }
}
