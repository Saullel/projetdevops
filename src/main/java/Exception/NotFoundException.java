package Exception;

public class NotFoundException extends Exception {
    public NotFoundException(String messageError){
        super(messageError);
    }
}
