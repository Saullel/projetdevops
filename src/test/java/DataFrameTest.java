import static org.junit.Assert.*;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.lang.NullArgumentException;
import org.junit.Test;
import org.junit.internal.runners.statements.Fail;

import Exception.InputErrorException;
import Exception.NotFoundException;
import junit.framework.Assert;


public class DataFrameTest {

	//Raw String [][] that will be used as data
	String SimpleGoodDataString[][]= {{"Laura","Francois","Julien","Thomas","Tiphaine","Tatiana"},{"Laulau","Fanfoué","Juju","Toto","Titi","Tata"}};
	String SimpleGoodDataStringOneSerie[][]= {{"Laura","Laulau","Francois","Fanfoué","Julien","Juju","Thomas","Toto","Tiphaine","Titi","Tatiana","Tata"}};
	String SimpleGoodDataStringOneLine[][]= {{"Fanfoué bg"},{"Laulau l'escargot"}};
	String SimpleGoodDataInt[][]= {{"1","1","2","2","3","3"},{"4","4","5","5","6","6"}};
	String SimpleGoodDataFloat[][]= {{"1.0","1.0","2.0","2.0","3.0","3.0"},{"4.0","4.0","5.0","5.0","6.0","6.0"}};
	String BadDataOneColBigger[][]={{"Laura","Francois","Julien","Thomas","Tiphaine","Tatiana"},{"Laulau","Fanfoué","Fanfouette","Juju","Toto","Titi","Tata"}};
	String BadDataOneColSmaller[][]={{"Laura","Francois","Julien","Thomas","Tiphaine","Tatiana"},{"Laulau","Juju","Toto","Titi","Tata"}};
	String BadDataOneNullValue[][]={{"Laura","Francois","Julien","Thomas","Tiphaine","Tatiana"},{"Laulau","","Juju","Toto","Titi","Tata"}};
	String BadDataNull[][]=null;
	//Raw String[] that will be used as labels
	String SimpleGoodLabels[]= {"Prenom","Surnom"};
	String BadLabelsTooMuch[]= {"Prenom","Surnom","Adresse"};
	String BadLabelsNotEnought[]= {"Prenom"};
	String BadLabelsOneNullValue[]= {"Prenom",""};
	String BadLabelNull[]=null;
	//Few csv files, the corresponding files are in /CsvTestFile/
	String BadDataEmptyFile="CsvTestFiles/BadDataEmptyFile.csv";
	String BadDataEmptyLabel="CsvTestFiles/BadDataEmptyLabel.csv";
	String BadDataEmptyValue="CsvTestFiles/BadDataEmptyValue.csv";
	String BadDataNotEnougthLabels="CsvTestFiles/BadDataNotEnougthLabels.csv";
	String BadDataOneLineBiggerWithLabels="CsvTestFiles/BadDataOneLineBiggerWithLabels.csv";
	String BadDataOneLineBiggerWithoutLabels="CsvTestFiles/BadDataOneLineBiggerWithoutLabels.csv";
	String BadDataOneLineSmallerWithLabels="CsvTestFiles/BadDataOneLineSmallerWithLabels.csv";
	String BadDataOneLineSmallerWithoutLabels="CsvTestFiles/BadDataOneLineSmallerWithoutLabels.csv";
	String BadDataTooMuchLabels="CsvTestFiles/BadDataTooMuchLabels.csv";
	String BadDataWithLabelButNoValue="CsvTestFiles/BadDataWithLabelButNoValue.csv";
	String GoodData="CsvTestFiles/GoodData.csv";
	String GoodDataWithLabels="CsvTestFiles/GoodDataWithLabels.csv";
	String BadDataNotExistingFile="CsvTestFiles/toto.csv";

	/*
	 * TESTS FOR public DataFrame(String[][] values) CONSTRUCTOR
	 */
	@Test 
	public void testDataFrameFromTab_correct_content() {
		try {
			DataFrame df = new DataFrame(SimpleGoodDataStringOneSerie);
				assertEquals("The content of the dataframe isn't correct", df.data.get(0), new Serie(SimpleGoodDataStringOneSerie[0],"default0"));
		} catch (InputErrorException e) {
			// Should not happend here
		}
	}
	
	@Test(expected=NullArgumentException.class)
	public void testDataFrameFromTab_value_null() throws NullArgumentException, InputErrorException{
		DataFrame df=new DataFrame(BadDataNull);
	}

	@Test(expected=InputErrorException.class)
	public void testDataFrameFromTab_serie_with_empty_value() throws NullArgumentException, InputErrorException {
		DataFrame df=new DataFrame(BadDataOneNullValue);
	}
	
	@Test(expected=InputErrorException.class)
	public void testDataFrameFromTab_serie_with_too_much_value() throws NullArgumentException, InputErrorException {
		DataFrame df=new DataFrame(BadDataOneColBigger);
	}
	
	@Test(expected=InputErrorException.class)
	public void testDataFrameFromTab_serie_with_not_enought_value() throws NullArgumentException, InputErrorException {
		DataFrame df=new DataFrame(BadDataOneColSmaller);
	}
	
	@Test
	public void testDataFrameFromTab_correct_typed_string_simple() {
		try {
			DataFrame df = new DataFrame(SimpleGoodDataString);
			assertEquals("The type of the dataframe serie isn't correct", df.getSerie(df.data.get(0).getLabel()).getType().toString(), "STRING");
		} catch (InputErrorException | NotFoundException e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testDataFrameFromTab_correct_typed_float_simple() {
		try {
			DataFrame df = new DataFrame(SimpleGoodDataFloat);
			assertEquals("The type of the dataframe serie isn't correct", df.getSerie(df.data.get(0).getLabel()).getType().toString(), "FLOAT");
		} catch (InputErrorException | NotFoundException e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testDataFrameFromTab_correct_typed_int_simple() {
		try {
			DataFrame df = new DataFrame(SimpleGoodDataInt);
			assertEquals("The type of the dataframe serie isn't correct", df.getSerie(df.data.get(0).getLabel()).getType().toString(), "INT");
		} catch (InputErrorException | NotFoundException e) {
			e.printStackTrace();
			fail();
		}
	}
	
	
	/*
	 * TESTS FOR public DataFrame(String[][] values, String[] labels) CONSTRUCTOR
	 */
	
	public void testDataFrameFromTabAndLabels_correct_content() {
		ArrayList<ArrayList<String>> list=new ArrayList<ArrayList<String>>();
		//We create an object which our dataframe will supposed to be equal
		for(int i=0;i<SimpleGoodDataString.length;i++) {
			ArrayList<String> l=new ArrayList<String>();
			for(int j=0;j<SimpleGoodDataString[i].length;j++)
				l.add(SimpleGoodDataString[i][j]);
			list.add(l);
		}		
		//We create the dataframe from the tab
		try {
			DataFrame df = new DataFrame(SimpleGoodDataString,SimpleGoodLabels);
			assertEquals("The content of the dataframe isn't correct", df, list);
		} catch (InputErrorException e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test(expected=NullArgumentException.class)
	public void testDataFrameFromTabAndLabels_value_null() throws NullArgumentException, InputErrorException{
		DataFrame df=new DataFrame(BadDataNull,SimpleGoodLabels);
	}

	@Test(expected=InputErrorException.class)
	public void testDataFrameFromTabAndLabels_serie_with_empty_value() throws NullArgumentException, InputErrorException {
		DataFrame df=new DataFrame(BadDataOneNullValue,SimpleGoodLabels);
	}
	
	@Test(expected=InputErrorException.class)
	public void testDataFrameFromTabAndLabels_serie_with_too_much_value() throws NullArgumentException, InputErrorException {
		DataFrame df=new DataFrame(BadDataOneColBigger,SimpleGoodLabels);
	}
	
	@Test(expected=InputErrorException.class)
	public void testDataFrameFromTabAndLabels_serie_with_not_enought_value() throws NullArgumentException, InputErrorException {
		DataFrame df=new DataFrame(BadDataOneColSmaller,SimpleGoodLabels);
	}
	//créer un tableau avec des colonnes dont le type est simple a déterminer
	@Test
	public void testDataFrameFromTabAndLabels_correct_typed_string_simple() {
		try {
			DataFrame df = new DataFrame(SimpleGoodDataString,SimpleGoodLabels);
			assertEquals("The type of the dataframe serie isn't correct", df.getSerie(df.data.get(0).getLabel()).getType().toString(), "STRING");
		} catch (InputErrorException | NotFoundException e) {
			// should not happend
		}
	}
	@Test
	public void testDataFrameFromTabAndLabels_correct_typed_float_simple() {
		try {
			DataFrame df = new DataFrame(SimpleGoodDataFloat,SimpleGoodLabels);
			assertEquals("The type of the dataframe serie isn't correct", df.getSerie(df.data.get(0).getLabel()).getType().toString(), "FLOAT");
		} catch (InputErrorException | NotFoundException e) {
			// should not happend
		}
	}
	@Test
	public void testDataFrameFromTabAndLabels_correct_typed_int_simple() {
		try {
			DataFrame df = new DataFrame(SimpleGoodDataInt,SimpleGoodLabels);
			assertEquals("The type of the dataframe serie isn't correct", df.getSerie(df.data.get(0).getLabel()).getType().toString(), "INT");
		} catch (InputErrorException | NotFoundException e) {
			// should not happend
		}
	}
	
	@Test(expected=NullArgumentException.class)
	public void testDataFrameFromTabAndLabels_label_null() throws NullArgumentException, InputErrorException{
		DataFrame df=new DataFrame(SimpleGoodDataString,BadLabelNull);
	}

	@Test(expected=InputErrorException.class)
	public void testDataFrameFromTabAndLabels_label_with_empty_value() throws NullArgumentException, InputErrorException {
		DataFrame df=new DataFrame(SimpleGoodDataString,BadLabelsOneNullValue);
	}
	
	@Test(expected=InputErrorException.class)
	public void testDataFrameFromTabAndLabels_too_much_label() throws NullArgumentException, InputErrorException {
		DataFrame df=new DataFrame(SimpleGoodDataString,BadLabelsTooMuch);
	}
	
	@Test(expected=InputErrorException.class)
	public void testDataFrameFromTabAndLabel_not_enought_label() throws NullArgumentException, InputErrorException {
		DataFrame df=new DataFrame(SimpleGoodDataString,BadLabelsNotEnought);
	}
	//@Test //TODO : problème avec l'alignement des labels qui fait qu'il sont pas considéré comme égaux
	public void testDataFrameFromTabAndLabels_correct_labels() {
		ArrayList<String> list_lab=new ArrayList<String>();
		for(int i=0;i<SimpleGoodLabels.length;i++) {
				list_lab.add(SimpleGoodLabels[i]);
		}		
		//We create the dataframe from the tab
		try {
			DataFrame df = new DataFrame(SimpleGoodDataString,SimpleGoodLabels);
			ArrayList<String> act_list_lab=new ArrayList<String>();
			for(Serie s: df.data)
				act_list_lab.add(s.getLabel().trim());
			System.out.println(act_list_lab);
			System.out.println(list_lab);
			assertEquals("The labels are not correct", list_lab, act_list_lab);
		} catch (InputErrorException e) {
			// Should not happend here
		}
	}
	
	/*
	 * TESTS FOR public DataFrame (String csvFile) throws IOException CONSTRUCTOR
	 */
	@Test
	public void testDataFrameFromCSV_correct_content() {
		try {
			DataFrame df=new DataFrame(GoodData);
			DataFrame df_expected=new DataFrame(SimpleGoodDataString);
			assertEquals("The two dataframes shall be equals",df_expected, df_expected);
			} catch (NullArgumentException | IOException | InputErrorException e) {
			fail();
		}
	}

	@Test(expected=InputErrorException.class)
	public void testDataFrameFromCSV_empty_file() throws NullArgumentException, IOException, InputErrorException {
		DataFrame df=new DataFrame(BadDataEmptyFile);
	}
	@Test(expected=IOException.class)
	public void testDataFrameFromCSV_not_existing_file() throws NullArgumentException, IOException, InputErrorException {
		DataFrame df=new DataFrame(BadDataNotExistingFile);
	}
	@Test(expected=InputErrorException.class)
	public void testDataFrameFromCSV_EmptyValue() throws NullArgumentException, IOException, InputErrorException {
		DataFrame df=new DataFrame(BadDataEmptyValue);
		//df.print();
	}
	@Test(expected=InputErrorException.class)
	public void testDataFrameFromCSV_OneLineBigger() throws NullArgumentException, IOException, InputErrorException {
		DataFrame df=new DataFrame(BadDataOneLineBiggerWithoutLabels);
	}
	@Test(expected=InputErrorException.class)
	public void testDataFrameFromCSV_OneLineSmaller() throws NullArgumentException, IOException, InputErrorException {
		DataFrame df=new DataFrame(BadDataOneLineSmallerWithoutLabels);
	}
	/*
	 * TESTS FOR public DataFrame (String csvFile,boolean islabel) throws IOException CONSTRUCTOR
	 */
	@Test
	public void testDataFrameFromCSVWithLabels_correct_content_and_labels() {
		try {
			DataFrame df=new DataFrame(GoodDataWithLabels,true);
			DataFrame df_expected=new DataFrame(SimpleGoodDataString,SimpleGoodLabels);
			assertEquals("The two dataframes shall be equals",df_expected, df);
			} catch (NullArgumentException | IOException | InputErrorException e) {
				e.printStackTrace();
				fail();
		}
	}
	public void testDataFrameFromCSVWithLabelsButWithoutLabel_correct_content_and_labels() {
		try {
			DataFrame df=new DataFrame(GoodDataWithLabels,false);
			DataFrame df_expected=new DataFrame(SimpleGoodDataString,SimpleGoodLabels);
			assertEquals("The two dataframes shall be equals",df_expected, df);
			} catch (NullArgumentException | IOException | InputErrorException e) {
				e.printStackTrace();
				fail();
		}
	}

	@Test(expected=InputErrorException.class)
	public void testDataFrameFromCSVWithLabels_EmptyFile() throws NullArgumentException, IOException, InputErrorException {
		DataFrame df=new DataFrame(BadDataEmptyFile,true);
	}
	@Test(expected=IOException.class)
	public void testDataFrameFromCSVWithLabels_not_existing_file() throws NullArgumentException, IOException, InputErrorException {
		DataFrame df=new DataFrame(BadDataNotExistingFile,true);
	}
	@Test(expected=InputErrorException.class)
	public void testDataFrameFromCSVWithLabels_EmptyValue() throws NullArgumentException, IOException, InputErrorException {
		DataFrame df=new DataFrame(BadDataEmptyValue,true);
	}
	@Test(expected=InputErrorException.class)
	public void testDataFrameFromCSVWithLabels_OneLineBigger() throws NullArgumentException, IOException, InputErrorException {
		DataFrame df=new DataFrame(BadDataOneLineBiggerWithLabels,true);
	}
	@Test(expected=InputErrorException.class)
	public void testDataFrameFromCSVWithLabels_OneLineSmaller() throws NullArgumentException, IOException, InputErrorException {
		DataFrame df=new DataFrame(BadDataOneLineSmallerWithLabels,true);
	}
	@Test(expected=InputErrorException.class)
	public void testDataFrameFromCSVWithLabels_EmptyLabel() throws NullArgumentException, IOException, InputErrorException {
		DataFrame df=new DataFrame(BadDataEmptyLabel,true);
	}
	@Test(expected=InputErrorException.class)
	public void testDataFrameFromCSVWithLabels_NotEnoughtLabels() throws NullArgumentException, IOException, InputErrorException {
		DataFrame df=new DataFrame(BadDataNotEnougthLabels,true);
	}
	@Test(expected=InputErrorException.class)
	public void testDataFrameFromCSVWithLabels_TooMuchLabels() throws NullArgumentException, IOException, InputErrorException {
		DataFrame df=new DataFrame(BadDataTooMuchLabels,true);
	}
	@Test(expected=InputErrorException.class)
	public void testDataFrameFromCSVWithLabels_CorrectLabels_but_NoValue() throws NullArgumentException, IOException, InputErrorException {
		DataFrame df=new DataFrame(BadDataWithLabelButNoValue,true);
	}
	
	/*
	 * TEST sur 	private String getLinesFromTo(int first, int last) throws IndexOutOfBoundsException{
	 */
	@Test
	public void testToString() {
		DataFrame df;
		try {
			df = new DataFrame(SimpleGoodDataString);
			assertEquals("\n" + 
					"	default0	default1\n" + 
					"0	   Laura	  Laulau	\n" + 
					"1	Francois	 Fanfoué	\n" + 
					"2	  Julien	    Juju	\n" + 
					"3	  Thomas	    Toto	\n" + 
					"4	Tiphaine	    Titi	\n" + 
					"5	 Tatiana	    Tata	\n" + 
					"",df.toString());
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
			}
	}
	/*
	 * public void head(int n) throws IndexOutOfBoundsException {
	 */
	@Test(expected=IndexOutOfBoundsException.class)
	public void test_head_badEndIndex() {
		DataFrame df;
		try {
			df = new DataFrame(SimpleGoodDataString);
			df.head(7);
			
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
			}
	}
	@Test(expected=IndexOutOfBoundsException.class)
	public void test_head_badEndIndexInverted() {
		DataFrame df;
		try {
			df = new DataFrame(SimpleGoodDataString);
			df.head(-1);
			
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
			}
	}
	
	/*
	 * TEST sur public void tail(int n) throws IndexOutOfBoundsException 
	 */
	@Test(expected=IndexOutOfBoundsException.class)
	public void test_tail_badBeginIndex() {
		DataFrame df;
		try {
			df = new DataFrame(SimpleGoodDataString);
			df.tail(-1);
			
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
			}
	}
	@Test(expected=IndexOutOfBoundsException.class)
	public void test_tail_badBeginIndexInverted() {
		DataFrame df;
		try {
			df = new DataFrame(SimpleGoodDataString);
			df.tail(7);
			
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
			}
	}
	/*
	 * TEST sur public void dtypes()
	 */
	@Test
	public void test_typeSelection_string() {
		DataFrame df;
		try {
			df = new DataFrame(SimpleGoodDataString,SimpleGoodLabels);
			assertEquals("STRING", df.data.get(0).getType());
			
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
			}
	}
	
	@Test
	public void test_typeSelection_float() {
		DataFrame df;
		try {
			df = new DataFrame(SimpleGoodDataFloat,SimpleGoodLabels);
			assertEquals("FLOAT", df.data.get(0).getType());
			
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
			}
	}
	
	@Test
	public void test_typeSelection_int() {
		DataFrame df;
		try {
			df = new DataFrame(SimpleGoodDataInt,SimpleGoodLabels);
			assertEquals("INT", df.data.get(0).getType());
			
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
			}
	}
	/*
	 * TEST sur equals
	 */
	@Test
	public void testEqualsBetweenTwoDataFrames() {
		try {
			DataFrame df1 = new DataFrame(SimpleGoodDataString);
			DataFrame df2=new DataFrame(SimpleGoodDataString);
			assertEquals("Les deux dataframes devraient etre égaux", df1,df2);
		} catch (NullArgumentException e) {
			e.printStackTrace();
			fail();
		} catch (InputErrorException e) {
			e.printStackTrace();
			fail();
		}
	}
	
	/*
	 * Test sur public Serie getSerie(String name) throws NotFoundException {
	 */
	@Test(expected=NotFoundException.class)
	public void testgetSerie_SerieDoesNotExist() throws NotFoundException {
		DataFrame df;
		try {
			df = new DataFrame(SimpleGoodDataInt,SimpleGoodLabels);
			df.getSerie("Sommeil");
			
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
			}
	}
	@Test
	public void testgetSerie_correctContent() throws NotFoundException {
		DataFrame df;
		try {
			df = new DataFrame(SimpleGoodDataStringOneSerie);
			assertEquals(df.data.get(0),df.getSerie("default0"));
			
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
			}
	}
	/*
	 * Test sur 	public DataFrame getSubData(int first, int last) throws IndexOutOfBoundsException, InputErrorException {
	 */
	@Test(expected=IndexOutOfBoundsException.class)
	public void testGetSubData_BadBeginIndex() throws NullArgumentException, InputErrorException {
		DataFrame df = new DataFrame(SimpleGoodDataString);
		df.getSubData(-1, 4);
	}
	@Test(expected=IndexOutOfBoundsException.class)
	public void testGetSubData_BadEndIndex() throws NullArgumentException, InputErrorException {
		DataFrame df = new DataFrame(SimpleGoodDataString);
		df.getSubData(1, 7);
	}
	@Test(expected=IndexOutOfBoundsException.class)
	public void testGetSubData_BadIndexInverted() throws NullArgumentException, InputErrorException {
		DataFrame df = new DataFrame(SimpleGoodDataString);
		df.getSubData(3, 1);
	}
	@Test
	public void testGetSubData_CorrectContent_OneLine() throws IndexOutOfBoundsException, InputErrorException {
		DataFrame df = new DataFrame(SimpleGoodDataStringOneLine);
		DataFrame df2=df.getSubData(0, df.data.get(0).size()-1);
		assertEquals(df,df2);
	}
	@Test
	public void testGetSubData_CorrectContent_FewSeries() throws NullArgumentException, InputErrorException {
		DataFrame df = new DataFrame(SimpleGoodDataString);
		DataFrame df2=df.getSubData(0, df.data.get(0).size()-1);
		assertEquals(df,df2);
	}
}
