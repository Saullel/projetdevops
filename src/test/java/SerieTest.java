import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import Exception.InputErrorException;
import Exception.TypeErrorException;

import static org.junit.Assert.*;

import org.apache.commons.lang.NullArgumentException;
/***
 * @auth
 */
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class SerieTest {
	// Create a stream to hold the output
	ByteArrayOutputStream baos;
	PrintStream ps;
	// IMPORTANT: Save the old System.out!
	PrintStream old;

	String[] val = {"Laura","François"};
	String[] val_6_elements = {"Laura","François","Sophie","Lyne","Martial","Robin"};
	String res = "\t    Name\n" + "0\t   Laura\n" + "1\tFrançois\n\n";
	String res_one_line_head = "\t    Name\n" + "0\t   Laura\n\n";
	String res_one_line_tail = "\t    Name\n" + "1\tFrançois\n\n";
	String res_only_label = "\t    Name\n\n";
	String res_tail_more_6_elem = "\t    Name\n" + "1\tFrançois\n" + "2\t  Sophie\n" + "3\t    Lyne\n" + "4\t Martial\n" + "5\t   Robin\n\n";
	String res_head_more_6_elem = "\t    Name\n" + "0\t   Laura\n" + "1\tFrançois\n" + "2\t  Sophie\n" + "3\t    Lyne\n" + "4\t Martial\n\n";

	String SimpleSerieString[]= {"lalala","lululu","lololo","lilili"};
	String SimpleSerieStringSmaller[]= {"lalala","lululu","lololo"};
	String SimpleSerieStringOneValue[]= {"lalala"};
	String SimpleSerieStringBigger[]= {"lalala","lululu","lololo","lilili","lelele"};
	String SimpleSerieStringDifferent[]= {"lalala","lululu","lololo","lelele"};
	String SimpleSerieInt[]= {"1","2","3","4"};
	String SimpleSerieIntOneNumber[]= {"1"};
	String SimpleSerieFloat[]= {"1.0","2.0","3.0","4.0"};
	String BadSerieNullVal[]= {"lalala","lululu","","lilili"};
	String BadSerieNull[]= null;

	String SimpleLabel="L-Word";
	String SimpleLabel2="L-Words";
	String EmptyLabel="";
	String NullLabel=null;

	private void getSysout(){
		baos = new ByteArrayOutputStream();
		ps = new PrintStream(baos);
		old = System.out;
		// Tell Java to use your special stream
		System.setOut(ps);
	}

	private void getBackSysout(){
		// Put things back
		System.out.flush();
		System.setOut(old);
	}

	/*
	 * Test sur public Serie(String[] values) throws NullArgumentException, InputErrorException
	 */
	@Test(expected=NullArgumentException.class)
	public void testSerieConstrcutorFromValues_ValNull() throws NullArgumentException, InputErrorException {
		Serie s=new Serie(BadSerieNull);
	}
	@Test(expected = InputErrorException.class)
	public void testSerieConstrcutorFromValues_OneValEmpty() throws NullArgumentException, InputErrorException {
		Serie s=new Serie(BadSerieNullVal);
	}
	@Test
	public void testSerieConstrcutorFromValues_CorrectContent() {
		Serie s;
		try {
			s=new Serie(SimpleSerieString);
			for(int i=0;i<SimpleSerieString.length;i++)
				assertEquals(SimpleSerieString[i], s.getVal(i).trim());
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
		}
	}
	@Test
	public void testSerieConstrcutorFromValues_CorrectType_int() {
		Serie s;
		try {
			s=new Serie(SimpleSerieInt);
			assertEquals("INT", s.getType());
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
		}
	}
	@Test
	public void testSerieConstrcutorFromValues_CorrectType_float() {
		Serie s;
		try {
			s=new Serie(SimpleSerieFloat);
			assertEquals("FLOAT", s.getType());
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
		}
	}
	@Test
	public void testSerieConstrcutorFromValues_CorrectType_String() {
		Serie s;
		try {
			s = new Serie(SimpleSerieString);
			assertEquals("STRING", s.getType());
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
		}
	}
	/*
	 * Test sur public Serie(String[] values, String label) throws NullArgumentException, InputErrorException
	 */
	@Test(expected=NullArgumentException.class)
	public void testSerieConstrcutorFromValuesAndLabel_ValNull() throws NullArgumentException, InputErrorException {
		Serie s=new Serie(BadSerieNull,SimpleLabel);
	}
	@Test(expected=NullArgumentException.class)
	public void testSerieConstrcutorFromValuesAndLabel_LabelNull() throws NullArgumentException, InputErrorException {
		Serie s=new Serie(SimpleSerieString,NullLabel);
	}
	@Test(expected = InputErrorException.class)
	public void testSerieConstrcutorFromValuesAndLabel_OneValEmpty() throws NullArgumentException, InputErrorException {
		Serie s=new Serie(BadSerieNullVal,SimpleLabel);
	}
	@Test(expected = InputErrorException.class)
	public void testSerieConstrcutorFromValuesAndLabel_LabelEmpty() throws NullArgumentException, InputErrorException {
		Serie s=new Serie(SimpleSerieString,EmptyLabel);
	}
	@Test
	public void testSerieConstrcutorFromValuesAndLabel_CorrectContent() {
		Serie s;
		try {
			s=new Serie(SimpleSerieString,SimpleLabel);
			assertEquals(SimpleLabel, s.getLabel());
			for(int i=0;i<SimpleSerieString.length;i++)
				assertEquals(SimpleSerieString[i], s.getVal(i).trim());
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
		}
	}
	@Test
	public void testSerieConstrcutorFromValuesAndLabel_CorrectType_int() {
		Serie s;
		try {
			s=new Serie(SimpleSerieInt);
			assertEquals("INT", s.getType());
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
		}
	}
	@Test
	public void testSerieConstrcutorFromValuesAndLabel_CorrectType_float() {
		Serie s;
		try {
			s=new Serie(SimpleSerieFloat);
			assertEquals("FLOAT", s.getType());
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
		}
	}
	@Test
	public void testSerieConstrcutorFromValuesAndLabel_CorrectType_String() {
		Serie s;
		try {
			s = new Serie(SimpleSerieString);
			assertEquals("STRING", s.getType());
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
		}
	}

	/*
	 * TEST sur  public String getType()
	 */
	@Test
	public void testSerieGetType_CorrectType_String() {
		Serie s;
		try {
			s = new Serie(SimpleSerieString);
			assertEquals("STRING", s.getType());
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
		}
	}
	@Test
	public void testSerieGetType_CorrectType_Int() {
		Serie s;
		try {
			s=new Serie(SimpleSerieInt);
			assertEquals("INT", s.getType());
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
		}
	}
	@Test
	public void testSerieGetType_CorrectType_Float() {
		Serie s;
		try {
			s=new Serie(SimpleSerieFloat);
			assertEquals("FLOAT", s.getType());
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
		}
	}
    @Test
    public void testGetlabel_default() {
        String[] val = {"aa"};
        Serie serie=new Serie();
		try {
			serie = new Serie(val);
		} catch (NullPointerException | InputErrorException e) {
			//will not happend
		}
        assertEquals("default", serie.getLabel());
    }
	/*
	 * Test sur  public boolean equals (Object o) 
	 */
    @Test
    public void testSerieEquals_diff_label() {
    	try {
			Serie s1=new Serie(SimpleSerieString,SimpleLabel);
	    	Serie s2=new Serie(SimpleSerieString,SimpleLabel2);
	    	assertNotSame(s2, s1);
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
		}
    }
    @Test
    public void testSerieEquals_diff_type() {
    	try {
			Serie s1=new Serie(SimpleSerieString,SimpleLabel);
	    	Serie s2=new Serie(SimpleSerieInt,SimpleLabel);
	    	assertNotSame(s2, s1);
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
		}
    }
    @Test
    public void testSerieEquals_diff_sizeBigger() {
    	try {
			Serie s1=new Serie(SimpleSerieString,SimpleLabel);
	    	Serie s2=new Serie(SimpleSerieStringBigger,SimpleLabel);
	    	assertNotSame(s2, s1);
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
		}
    }
    @Test
    public void testSerieEquals_diff_sizeSmaller() {
    	try {
			Serie s1=new Serie(SimpleSerieString,SimpleLabel);
	    	Serie s2=new Serie(SimpleSerieStringSmaller,SimpleLabel);
	    	assertNotSame(s2, s1);
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
		}
    }
    @Test
    public void testSerieEquals_diff_value() {
    	try {
			Serie s1=new Serie(SimpleSerieString,SimpleLabel);
	    	Serie s2=new Serie(SimpleSerieStringDifferent,SimpleLabel);
	    	assertNotSame(s2, s1);
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
		}
    }
    @Test
    public void testSerieEquals_sameSeries() {
    	try {
			Serie s1=new Serie(SimpleSerieString,SimpleLabel);
	    	Serie s2=new Serie(SimpleSerieString,SimpleLabel);
	    	assertEquals(s2, s1);
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
		}
    }
    /*
     *Tests sur private String getLinesFromTo(int first, int last) throws IndexOutOfBoundsException
     */
    @Test
	public void testToString() {
		Serie s;
		try {
			s = new Serie(SimpleSerieString);
			assertEquals("	default\n"+
					"0	 lalala\n"+
					"1	 lululu\n"+
					"2	 lololo\n"+
					"3	 lilili\n"+
"",s.toString());
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
			}
	}

	/*
	 * TEST sur public void head() throws IndexOutOfBoundsException
	 */
	@Test
	public void testHead_less_5_elements(){
		getSysout();
		try {
			Serie serie = new Serie(val,"Name");
			serie.head();
			getBackSysout();
			assertEquals(res,baos.toString());
		} catch (InputErrorException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testHead_more_5_elements(){
		getSysout();
		try {
			Serie serie = new Serie(val_6_elements,"Name");
			serie.head();
			getBackSysout();
			assertEquals(res_head_more_6_elem,baos.toString());
		} catch (InputErrorException e) {
			e.printStackTrace();
		}
	}

	/*
	 * TEST sur public void head(int n) throws IndexOutOfBoundsException
	 */
	@Test(expected=IndexOutOfBoundsException.class)
	public void test_head_badEndIndex() {
		Serie s;
		try {
			s = new Serie(SimpleSerieString);
			s.head(7);
			
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
			}
	}
	@Test(expected=IndexOutOfBoundsException.class)
	public void test_head_badEndIndexInverted() {
		Serie s;
		try {
			s = new Serie(SimpleSerieString);
			s.head(-1);
			
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
			}
	}

	@Test
	public void testHead_with_correct_n_nothing_to_print(){
		getSysout();

		try {
			Serie serie = new Serie(val,"Name");
			serie.head(0);
			getBackSysout();

			assertEquals(res_only_label,baos.toString());
		} catch (InputErrorException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testHead_with_correct_n(){
		getSysout();

		try {
			Serie serie = new Serie(val,"Name");
			serie.head(1);
			getBackSysout();

			assertEquals(res_one_line_head,baos.toString());
		} catch (InputErrorException e) {
			e.printStackTrace();
		}
	}


	/*
	 * TEST sur public void tail() throws IndexOutOfBoundsException
	 */
	@Test
	public void testTail_less_5_elements(){
		getSysout();
		try {
			Serie serie = new Serie(val,"Name");
			serie.tail();
			getBackSysout();
			assertEquals(res,baos.toString());
		} catch (InputErrorException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testTail_more_5_elements(){
		getSysout();
		try {
			Serie serie = new Serie(val_6_elements,"Name");
			serie.tail();
			getBackSysout();
			assertEquals(res_tail_more_6_elem,baos.toString());
		} catch (InputErrorException e) {
			e.printStackTrace();
		}
	}

	/*
	 * TEST sur public void tail(int n) throws IndexOutOfBoundsException 
	 */
	@Test(expected=IndexOutOfBoundsException.class)
	public void test_tail_badBeginIndex() {
		Serie s;
		try {
			s = new Serie(SimpleSerieString);
			s.tail(-1);
			
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
			}
	}
	@Test(expected=IndexOutOfBoundsException.class)
	public void test_tail_badBeginIndexInverted() {
		Serie s;
		try {
			s = new Serie(SimpleSerieString);
			s.tail(7);
			
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
			}
	}

	@Test
	public void testTail_with_correct_n_nothing_to_print(){
		getSysout();

		try {
			Serie serie = new Serie(val,"Name");
			serie.tail(0);
			getBackSysout();

			assertEquals(res_only_label,baos.toString());
		} catch (InputErrorException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testTail_with_correct_n(){
		getSysout();

		try {
			Serie serie = new Serie(val,"Name");
			serie.tail(1);
			getBackSysout();

			assertEquals(res_one_line_tail,baos.toString());
		} catch (InputErrorException e) {
			e.printStackTrace();
		}
	}


	/*
	 * TEST sur print()
	 */
	@Test
	public void testTPrint(){
		getSysout();
		try {
			Serie serie = new Serie(val,"Name");
			serie.print();
			getBackSysout();
			assertEquals(res,baos.toString());

		} catch (InputErrorException e) {
			e.printStackTrace();
		}
	}


    /*
     * test sur    public Serie getSubSerie(int first, int last) throws IndexOutOfBoundsException {
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetSubSerie_BadBeginIndex() throws NullArgumentException, InputErrorException {
    	Serie s = new Serie(SimpleSerieString);
    	Serie s2 =s.getSubSerie(-1, 2);
    }
    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetSubSerie_BadEndIndex() throws NullArgumentException, InputErrorException {
    	Serie s = new Serie(SimpleSerieString);
    	Serie s2 =s.getSubSerie(0, 8);
    }
    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetSubSerie_BadIndexInverted() throws NullArgumentException, InputErrorException {
    	Serie s = new Serie(SimpleSerieString);
    	Serie s2 =s.getSubSerie(4, 2);
    }
    @Test
    public void testGetSubSerie_GoodSelection_OneLine() {
    	try {
	    	Serie s= new Serie(SimpleSerieStringOneValue);
	    	Serie s2 =new Serie(SimpleSerieStringSmaller);
	    	assertEquals(s, s2.getSubSerie(0, 0));
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
		}
    }
    @Test
    public void testGetSubSerie_GoodSelection_FewLines() {
		try {
	    	Serie s= new Serie(SimpleSerieStringSmaller);
	    	Serie s2 =new Serie(SimpleSerieStringBigger);
	    	assertEquals(s, s2.getSubSerie(0, 2));
		} catch (NullArgumentException | InputErrorException e) {
			e.printStackTrace();
			fail();
		}
    }
    @Test
    public void testGetLabel() {
        String[] val = {"aa"};
        Serie serie=new Serie();
		try {
			serie = new Serie(val,"test");
		} catch (NullArgumentException | InputErrorException e) {
			// will not happend
		}
        assertEquals("test", serie.getLabel());
    }

    @Test
    public void testSetLabel() {
        String[] val = {"aa"};
        Serie serie=new Serie();
		try {
			serie = new Serie(val, "label");
		} catch (NullArgumentException | InputErrorException e) {
			// will not happend
		}
        serie.setLabel("test");
        assertEquals("test", serie.getLabel());
    }

    @Test
    public void testCount_notEmpty(){
        String[] val = {"aa","1"};
        Serie serie=new Serie();
		try {
			serie = new Serie(val, "label");
		} catch (NullArgumentException | InputErrorException e) {
			// will not happend
		}
        assertEquals(2,serie.count(),0);
    }

    @Test
    public void testCount_empty(){
        String[] val = {};
        Serie serie=new Serie();
		try {
			serie = new Serie(val, "label");
		} catch (NullArgumentException | InputErrorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        assertEquals(0,serie.count(),0);
    }

    /*
     * Test sur public double mean() throws TypeErrorException {
     */
    @Test
    public void testMean_goodValue() throws NullArgumentException, InputErrorException, TypeErrorException {
    	Serie s= new Serie(SimpleSerieInt);
    	assertEquals(new Double(2.5),new Double(s.mean()));
    }
    @Test
    public void testMean_goodValueOneValue() throws NullArgumentException, InputErrorException, TypeErrorException {
    	Serie s= new Serie(SimpleSerieIntOneNumber);
    	assertEquals(new Double(1),new Double(s.mean()));
    }
    @Test
    public void testMean_goodValueFromFloat() throws NullArgumentException, InputErrorException, TypeErrorException {
    	Serie s= new Serie(SimpleSerieFloat);
    	assertEquals(new Double(2.5),new Double(s.mean()));
    }
    @Test(expected = TypeErrorException.class)
    public void testMean_BadType() throws NullArgumentException, InputErrorException, TypeErrorException {
    	Serie s= new Serie(SimpleSerieStringSmaller);
    	double moy=s.mean();
    }

    /*
     * Test sur  public double sd() throws TypeErrorException {
     */
    @Test
    public void testSd_goodValue() throws NullArgumentException, InputErrorException, TypeErrorException {
    	Serie s= new Serie(SimpleSerieInt);
    	assertEquals(new Double(1.118033988749895),new Double(s.sd()));
    }
    @Test
    public void testSd_goodValueOneValue() throws NullArgumentException, InputErrorException, TypeErrorException {
    	Serie s= new Serie(SimpleSerieIntOneNumber);
    	assertEquals(new Double(0.0),new Double(s.sd()));
    }
    @Test
    public void testSd_goodValueFromFloat() throws NullArgumentException, InputErrorException, TypeErrorException {
    	Serie s= new Serie(SimpleSerieFloat);
    	assertEquals(new Double(1.118033988749895),new Double(s.sd()));
    }
    @Test(expected = TypeErrorException.class)
    public void testSd_BadType() throws NullArgumentException, InputErrorException, TypeErrorException {
    	Serie s= new Serie(SimpleSerieStringSmaller);
    	double moy=s.sd();
    }
    /*
     * Test sur  public double min() throws TypeErrorException {
     */
    
    @Test
    public void testMin_goodValue() throws NullArgumentException, InputErrorException, TypeErrorException {
    	Serie s= new Serie(SimpleSerieInt);
    	assertEquals(new Double(1),new Double(s.min()));
    }
    @Test
    public void testMin_goodValueOneValue() throws NullArgumentException, InputErrorException, TypeErrorException {
    	Serie s= new Serie(SimpleSerieIntOneNumber);
    	assertEquals(new Double(1.0),new Double(s.min()));
    }
    @Test(expected = TypeErrorException.class)
    public void testMin_BadType() throws NullArgumentException, InputErrorException, TypeErrorException {
    	Serie s= new Serie(SimpleSerieStringSmaller);
    	double min=s.min();
    }
    /*
     * Test sur  public double max() throws TypeErrorException {
     */
    @Test
    public void testMax_goodValue() throws NullArgumentException, InputErrorException, TypeErrorException {
    	Serie s= new Serie(SimpleSerieInt);
    	assertEquals(new Double(4),new Double(s.max()));
    }
    @Test
    public void testMax_goodValueOneValue() throws NullArgumentException, InputErrorException, TypeErrorException {
    	Serie s= new Serie(SimpleSerieIntOneNumber);
    	assertEquals(new Double(1.0),new Double(s.max()));
    }
    @Test(expected = TypeErrorException.class)
    public void testMax_BadType() throws NullArgumentException, InputErrorException, TypeErrorException {
    	Serie s= new Serie(SimpleSerieStringSmaller);
    	double max=s.max();
    }
}